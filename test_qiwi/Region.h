//
//  Region.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 02.06.13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Unit;

@interface Region : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * regionId;
@property (nonatomic, retain) NSString * firstLetter;
@property (nonatomic, retain) NSSet *units;
@end

@interface Region (CoreDataGeneratedAccessors)

- (void)addUnitsObject:(Unit *)value;
- (void)removeUnitsObject:(Unit *)value;
- (void)addUnits:(NSSet *)values;
- (void)removeUnits:(NSSet *)values;

@end
