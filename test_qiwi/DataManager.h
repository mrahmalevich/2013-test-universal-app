//
//  DataManager.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 27.05.13.
//
//

#import <Foundation/Foundation.h>

@class ASIHTTPRequest, ASINetworkQueue;
@interface DataManager : NSObject {
@private
    ASIHTTPRequest *_currentOperation;
    ASINetworkQueue *_unitRequestsQueue;
}

+ (DataManager *)sharedInstance;

- (BOOL)checkReachability;

- (void)getRegions;
- (void)getUnitsForRegion:(Region *)region;
- (BOOL)isUnitsRequestComplete;
- (void)getDetailsForUnit:(Unit *)unit;

- (void)cancelCurrentOperation;

@end
