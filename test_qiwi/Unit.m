//
//  Unit.m
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 27.05.13.
//
//

#import "Unit.h"
#import "Region.h"
#import "UnitDetails.h"


@implementation Unit

@dynamic name;
@dynamic unitId;
@dynamic unitType;
@dynamic region;
@dynamic details;

@end
