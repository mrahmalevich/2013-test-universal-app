//
//  AbstractViewController.m
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 27.05.13.
//
//

#import "AbstractViewController.h"
#import "DataManager.h"

#define kBusyAlertView  100

@interface AbstractViewController ()

@end

@implementation AbstractViewController

#pragma mark - rotations
- (NSUInteger)supportedInterfaceOrientations
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    } else {
        return YES;
    }
}

#pragma mark - public methods
- (void)showBusyAlertWithText:(NSString *)text
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:text message:@"\n\n" delegate:self cancelButtonTitle:@"Отмена" otherButtonTitles:nil];
    [alertView setTag:kBusyAlertView];
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [loading startAnimating];
    [loading setUserInteractionEnabled:YES];
    [alertView show];
    
    CGPoint center = [alertView convertPoint:alertView.center fromView:alertView.superview];
    center.y -= 10;
    [loading setCenter:center];
    [alertView addSubview:loading];
    
    _busyAlertView = alertView;
}

- (void)dismissBusyAlert
{
    [_busyAlertView dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)showConnectionErrorAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Внимание!" message:@"Для загрузки данных необходимо активное wifi соединение. Подключитесь к сети и повторите попытку." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kBusyAlertView) {
        [[DataManager sharedInstance] cancelCurrentOperation];
    }
}

#pragma mark - actions
- (IBAction)actionClose:(UIBarButtonItem *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
