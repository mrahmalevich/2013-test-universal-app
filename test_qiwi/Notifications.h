//
//  Notifications.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 27.05.13.
//
//

#define kNotificationRegionsRequestSucceed      @"kNotificationRegionsRequestSucceed"
#define kNotificationRegionsRequestFailed       @"kNotificationRegionsRequestFailed"
#define kNotificationUnitsRequestSucceed        @"kNotificationUnitsRequestSucceed"
#define kNotificationUnitsRequestFailed         @"kNotificationUnitsRequestFailed"
#define kNotificationUnitDetailsRequestSucceed  @"kNotificationUnitDetailsRequestSucceed"
#define kNotificationUnitDetailsRequestFailed   @"kNotificationUnitDetailsRequestFailed"