//
//  NetworkHelper.m
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 27.05.13.
//
//

#import "DataManager.h"
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "Reachability.h"
#import "Notifications.h"

#define kBasicURL @"https://w.qiwi.ru/term2/xmlutf.jsp"

static DataManager *_sharedInstance = nil;
@implementation DataManager

#pragma mark - public methods
+ (DataManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[DataManager alloc] init];
    });
    return _sharedInstance;
}

- (BOOL)checkReachability
{
    return ([Reachability reachabilityForInternetConnection].currentReachabilityStatus == ReachableViaWiFi);
}

- (void)getRegions
{
    // we remove all stored regions and connected objects
    [Region MR_deleteAllMatchingPredicate:nil];
    
    // construct request
    NSURL *requestURL = [NSURL URLWithString:kBasicURL];
    __weak ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:requestURL];
    NSMutableDictionary *requestBody = [NSMutableDictionary dictionary];
    [requestBody setObject:@{@"request-type": @"493"} forKey:@"request"];
    [request appendPostData:[[requestBody xmlString] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setCompletionBlock:^{
        NSDictionary *responseDict = [NSDictionary dictionaryWithXMLData:request.responseData];
        [self loadRegionsFromDict:responseDict];
    }];
    [request setFailedBlock:^{
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:kNotificationRegionsRequestFailed object:nil]];
    }];
    [request startAsynchronous];
    
    // store operation pointer
    _currentOperation = request;
}

- (void)getUnitsForRegion:(Region *)region
{
    // we remove stored units
    [Unit MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"region = %@", region]];
    
    // construct request queue
    NSArray *unitTypes = @[@"exm", @"reg", @"gto", @"iaz"];
    ASINetworkQueue *requestsQueue = [ASINetworkQueue queue];
    [requestsQueue setDelegate:self];
    [requestsQueue setRequestDidStartSelector:@selector(unitsRequestDidStart:)];
    [requestsQueue setShouldCancelAllRequestsOnFailure:YES];
    [requestsQueue setMaxConcurrentOperationCount:1];
    
    // add requests to queue
    for (NSString *type in unitTypes) {
        NSURL *requestURL = [NSURL URLWithString:kBasicURL];
        __weak ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:requestURL];
        NSMutableDictionary *requestBody = [NSMutableDictionary dictionary];
        [requestBody setObject: @{
                                    @"request-type": @"462",
                                    @"extra": @[
                                        @{
                                            @"__attributes": @{
                                                @"name": @"region"
                                            },
                                            @"__text": region.regionId.stringValue
                                        },
                                        @{
                                            @"__attributes": @{
                                                @"name": @"type"
                                            },
                                            @"__text": type
                                        },
                                    ]
                                }
                        forKey:@"request"];
        NSString *xmlString = [requestBody xmlString];
        [request appendPostData:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
        [request setCompletionBlock:^{
            NSDictionary *responseDict = [NSDictionary dictionaryWithXMLData:request.responseData];
            [self loadUnitsForRegion:region fromDict:responseDict];
        }];
        [request setFailedBlock:^{
            [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:kNotificationUnitsRequestFailed object:nil]];
        }];
        [requestsQueue addOperation:request];
    }
    [requestsQueue go];
    
    // store queue pointer
    _unitRequestsQueue = requestsQueue;
}

- (void)unitsRequestDidStart:(ASIHTTPRequest *)request
{
    _currentOperation = request;
}

- (BOOL)isUnitsRequestComplete
{
    return (_unitRequestsQueue.requestsCount == 0);
}

- (void)getDetailsForUnit:(Unit *)unit
{
    // we remove stored details
    [UnitDetails MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"unit = %@", unit]];
    
    // construct request
    NSURL *requestURL = [NSURL URLWithString:kBasicURL];
    __weak ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:requestURL];
    NSMutableDictionary *requestBody = [NSMutableDictionary dictionary];
    [requestBody setObject: @{
                                @"request-type": @"462",
                                @"extra": @[
                                    @{
                                        @"__attributes": @{
                                            @"name": @"region"
                                        },
                                        @"__text": unit.region.regionId.stringValue
                                    },
                                    @{
                                        @"__attributes": @{
                                            @"name": @"type"
                                        },
                                        @"__text": unit.unitType
                                    },
                                    @{
                                        @"__attributes": @{
                                            @"name": @"div"
                                        },
                                        @"__text": unit.unitId.stringValue
                                    }
                                ]
                            }
                    forKey:@"request"];
    [request appendPostData:[[requestBody xmlString] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setCompletionBlock:^{
        NSDictionary *responseDict = [NSDictionary dictionaryWithXMLData:request.responseData];
        [self loadDetailsForUnit:unit fromDictionary:responseDict];
    }];
    [request setFailedBlock:^{
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:kNotificationUnitDetailsRequestFailed object:nil]];
    }];
    [request startAsynchronous];
    
    // store operation pointer
    _currentOperation = request;
}

- (void)cancelCurrentOperation
{
    [_currentOperation cancel];
}

#pragma mark - response processing
- (void)loadRegionsFromDict:(NSDictionary *)regionsDict
{
    if ([[regionsDict valueForKeyPath:@"result-code.__text"] integerValue] != 0) {
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:kNotificationRegionsRequestFailed object:nil]];
    } else {
        [MagicalRecord saveUsingCurrentThreadContextWithBlock:^(NSManagedObjectContext *localContext){
            NSArray *regionsArray = [regionsDict valueForKeyPath:@"list.info"];
            for (NSDictionary *regionDict in regionsArray) {
                Region *newRegion = [Region MR_createInContext:localContext];
                newRegion.regionId = [NSNumber numberWithInteger:[[regionDict objectForKey:@"_id"] integerValue]];;
                newRegion.name = [regionDict objectForKey:@"_name"];
                newRegion.firstLetter = [[[regionDict objectForKey:@"_name"] substringToIndex:1] uppercaseString];
            }
        } completion:^(BOOL success, NSError *error){
            NSString *notificationName = success ? kNotificationRegionsRequestSucceed : kNotificationRegionsRequestFailed;
            [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:notificationName object:error]];
        }];
    }
}

- (void)loadUnitsForRegion:(Region *)region fromDict:(NSDictionary *)unitsDict
{
    if ([[unitsDict valueForKeyPath:@"result-code.__text"] integerValue] != 0) {
        [[NSManagedObjectContext MR_defaultContext] rollback];
        [_unitRequestsQueue cancelAllOperations];
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:kNotificationUnitsRequestFailed object:nil]];
    } else {
        [MagicalRecord saveUsingCurrentThreadContextWithBlock:^(NSManagedObjectContext *localContext){
            id units = [unitsDict valueForKeyPath:@"infos.info"];
            NSArray *unitsArray = nil;
            if ([units isKindOfClass:[NSDictionary class]]) {
                unitsArray = @[units];
            } else {
                unitsArray = units;
            }
            for (NSDictionary *unitDict in unitsArray) {
                Unit *newUnit = [Unit MR_createInContext:localContext];
                newUnit.unitId = [NSNumber numberWithInteger:[[unitDict objectForKey:@"_subdivision_code"] integerValue]];
                newUnit.name = [unitDict objectForKey:@"_subdivision"];
                newUnit.unitType = [unitDict objectForKey:@"_type"];
                newUnit.region = region;
            }
        } completion:^(BOOL success, NSError *error){
            if (success) {
                if (_unitRequestsQueue.requestsCount == 0) {
                    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:kNotificationUnitsRequestSucceed object:nil]];
                }
            } else {
                [_unitRequestsQueue cancelAllOperations];
                [[NSNotificationCenter defaultCenter] postNotification:[NSNotification  notificationWithName:kNotificationUnitsRequestFailed object:error]];
            }
        }];
    }
}

- (void)loadDetailsForUnit:(Unit *)unit fromDictionary:(NSDictionary *)detailsDict
{
    if ([[detailsDict valueForKeyPath:@"result-code.__text"] integerValue] != 0) {
        [[NSManagedObjectContext MR_defaultContext] rollback];
        [_unitRequestsQueue cancelAllOperations];
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:kNotificationUnitDetailsRequestFailed object:nil]];
    } else {
        [MagicalRecord saveUsingCurrentThreadContextWithBlock:^(NSManagedObjectContext *localContext){
            NSDictionary *unitInfoDict = [detailsDict valueForKeyPath:@"infos.info"];
            UnitDetails *newDetails = [UnitDetails MR_createInContext:localContext];
            newDetails.to_name_f = [unitInfoDict valueForKey:@"_to_name_f"];
            newDetails.cre = [unitInfoDict valueForKey:@"_cre"];
            newDetails.kbk = [unitInfoDict valueForKey:@"_kbk"];
            newDetails.kpp = [unitInfoDict valueForKey:@"_kpp"];
            newDetails.okato = [unitInfoDict valueForKey:@"_okato"];
            newDetails.rec_bik = [unitInfoDict valueForKey:@"_rec_bik"];
            newDetails.inn = [unitInfoDict valueForKey:@"_inn"];
            newDetails.to_bank = [unitInfoDict valueForKey:@"_to_bank"];
            newDetails.unit = unit;
        } completion:^(BOOL success, NSError *error){
            NSString *notificationName = success ? kNotificationUnitDetailsRequestSucceed : kNotificationUnitDetailsRequestFailed;
            [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:notificationName object:nil]];
        }];
    }
}

@end
