//
//  UnitDetailsViewController.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 03.06.13.
//
//

#import "AbstractViewController.h"

@interface UnitDetailsViewController : AbstractViewController

@property (nonatomic, strong) Unit *unit;
@property (weak, nonatomic) IBOutlet UIScrollView *svContent;
@property (weak, nonatomic) IBOutlet UILabel *lblSubdivision;
@property (weak, nonatomic) IBOutlet UILabel *lblReciever;
@property (weak, nonatomic) IBOutlet UILabel *lblBank;
@property (weak, nonatomic) IBOutlet UILabel *lblBIK;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblKBK;
@property (weak, nonatomic) IBOutlet UILabel *lblKPP;
@property (weak, nonatomic) IBOutlet UILabel *lblOKATO;
@property (weak, nonatomic) IBOutlet UILabel *lblINN;

- (IBAction)actionRefresh:(UIBarButtonItem *)sender;

@end
