//
//  IPadMenuViewController.m
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 03.06.13.
//
//

#import "IPadMenuViewController.h"
#import "UnitsViewController.h"

@interface IPadMenuViewController ()

@end

@implementation IPadMenuViewController

- (void)selectRegion:(Region *)region
{
    // we search units view controller in child controllers
    UnitsViewController *unitsController = nil;
    for (UIViewController *controller in self.childViewControllers) {
        if ([controller isKindOfClass:[UnitsViewController class]]) {
            unitsController = (UnitsViewController *)controller;
            break;
        }
    }
    
    // then load region
    [unitsController setRegion:region];
}

@end
