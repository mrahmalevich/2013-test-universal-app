//
//  IPadMenuViewController.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 03.06.13.
//
//

#import "AbstractViewController.h"

@interface IPadMenuViewController : AbstractViewController

- (void)selectRegion:(Region *)region;

@end
