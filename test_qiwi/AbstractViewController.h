//
//  AbstractViewController.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 27.05.13.
//
//

#import <UIKit/UIKit.h>

@interface AbstractViewController : UIViewController <UIAlertViewDelegate> {
    UIAlertView *_busyAlertView;
}

- (NSUInteger)supportedInterfaceOrientations;
- (BOOL)shouldAutorotate;
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation;

- (void)showConnectionErrorAlert;
- (void)showBusyAlertWithText:(NSString *)text;
- (void)dismissBusyAlert;

- (IBAction)actionClose:(UIBarButtonItem *)sender;

@end
