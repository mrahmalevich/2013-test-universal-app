//
//  AppDelegate.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 27.05.13.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
