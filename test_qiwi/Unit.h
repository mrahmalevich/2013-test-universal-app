//
//  Unit.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 27.05.13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Region, UnitDetails;

@interface Unit : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * unitId;
@property (nonatomic, retain) NSString * unitType;
@property (nonatomic, retain) Region *region;
@property (nonatomic, retain) UnitDetails *details;

@end
