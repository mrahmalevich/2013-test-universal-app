//
//  Region.m
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 02.06.13.
//
//

#import "Region.h"
#import "Unit.h"


@implementation Region

@dynamic name;
@dynamic regionId;
@dynamic firstLetter;
@dynamic units;

@end
